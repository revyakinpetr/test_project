# -*- coding: utf-8 -*-

import textwrap

from django.db import models
from django.contrib.auth.models import User
from typing_extensions import Final, final

#: That's how constants should be defined.
_POST_TITLE_MAX_LENGTH: Final = 80


@final
class Advert(models.Model):
    """
    Represents :term:`Advert`
    """

    author = models.OneToOneField(User, on_delete=models.CASCADE, related_name='advert_author_user')
    executor = models.OneToOneField(User, on_delete=models.CASCADE, related_name='advert_executor_user', default=None)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    job_date = models.DateField()

    def __str__(self):
        """String representation of Collection."""
        return '{0}. {1}'.format(self.id, self.title)


@final
class AdvertResponse(models.Model):
    """
    Represents :term:`AdvertResponse`
    тклик: автор, объявление, комментарий, дата создания
    """

    author = models.OneToOneField(User, on_delete=models.CASCADE)
    advert = models.OneToOneField(Advert, on_delete=models.CASCADE)
    comment = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """String representation of Collection."""
        return '{0}. {1}'.format(self.id, self.title)
