# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from rest_framework import serializers
from server.apps.main.models import Advert, AdvertResponse


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['username']


class AdvertSerializer(serializers.ModelSerializer):
    """Serializer for Advert class."""
    class Meta:
        model = Advert
        fields = ('id', 'author', 'executor', 'title', 'description', 'created_at', 'job_date')


class AdvertResponseSerializer(serializers.ModelSerializer):
    """Serializer for Advert class."""
    class Meta:
        model = AdvertResponse
        fields = ('id', 'author', 'advert', 'comment', 'created_at')
