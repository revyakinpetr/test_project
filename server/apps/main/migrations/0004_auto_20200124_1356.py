# Generated by Django 2.2.9 on 2020-01-24 13:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20200124_1349'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advert',
            name='job_date',
            field=models.DateField(),
        ),
    ]
