# Generated by Django 2.2.9 on 2020-01-24 13:49

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20200124_1251'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advert',
            name='executor',
            field=models.OneToOneField(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='advert_executor_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
