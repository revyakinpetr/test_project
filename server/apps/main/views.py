# -*- coding: utf-8 -*-

from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User

from server.apps.main.models import Advert, AdvertResponse
from server.apps.main.serializers import AdvertSerializer, AdvertResponseSerializer, UserSerializer

from rest_framework import viewsets

# Create your views here.


def index(request: HttpRequest) -> HttpResponse:
    """
    Main (or index) view.

    Returns rendered default page to the user.
    Typed with the help of ``django-stubs`` project.
    """
    return render(request, 'main/index.html')


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class AdvertViewSet(viewsets.ModelViewSet):
    """
    REST API Viewset for Advert class.
    """
    queryset = Advert.objects.all()
    serializer_class = AdvertSerializer


class AdvertResponseViewSet(viewsets.ModelViewSet):
    """
    REST API Viewset for Advert class.
    """
    queryset = AdvertResponse.objects.all()
    serializer_class = AdvertResponseSerializer
