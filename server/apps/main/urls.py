# -*- coding: utf-8 -*-

from django.urls import include, path

from rest_framework import routers
from server.apps.main.views import index, AdvertViewSet, AdvertResponseViewSet, UserViewSet

# Place your URLs here:

app_name = 'main'
router = routers.DefaultRouter()
router.register(r'adverts', AdvertViewSet)
router.register(r'advertsResponse', AdvertResponseViewSet)
router.register(r'users', UserViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
