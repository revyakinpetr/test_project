import { AxiosInstance } from 'axios'
import * as ts from 'io-ts'
import * as tPromise from 'io-ts-promise'
import { Service, Container } from 'vue-typedi'

import { Advert, AdvertType } from '~/logic/advert/models'
import tokens from '~/logic/tokens'

@Service(tokens.ADVERT_SERVICE)
export default class AdvertService {
  protected get $axios (): AxiosInstance {
    return Container.get(tokens.AXIOS) as AxiosInstance
  }

  // Метод для загрузки JSON'ов с бэкенда
  public async fetchAdverts (): Promise<AdvertType[]> {
    const response = await this.$axios.get('advert/')
    return tPromise.decode(ts.array(Advert), response.data)
  }

  // Метод для обновления коллекции
  public async updateCollection (
    advert: AdvertType
  ): Promise<AdvertType> {
    const response = await this
      .$axios.put(`advert/${advert.id}/`, advert)
    return tPromise.decode(Advert, response.data)
  }
}
