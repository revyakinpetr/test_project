import { Inject, Injectable } from 'vue-typedi'
import { Action, Mutation, State, Getter } from 'vuex-simple'

import { AdvertType } from '~/logic/advert/models'
import AdvertService from '~/logic/advert/services/api'
import tokens from '~/logic/tokens'

@Injectable()
export default class AdvertModule {
  // Dependencies

  @Inject(tokens.ADVERT_SERVICE)
  public service!: AdvertService

  // State

  @State()
  public collections: AdvertType[] = []

  // Getters

  @Getter()
  public get hasCollections (): boolean {
    return Boolean(this.collections && this.collections.length > 0)
  }

  // Mutations

  @Mutation()
  public setCollections (payload: AdvertType[]): void {
    this.collections = payload
  }

  @Mutation()
  public updateCollectionTitle (id: number, title: string): void {
    if (!this.collections) return

    const commentIndex = this.collections.findIndex((collection): boolean => {
      return collection.id === id
    })

    if (!this.collections || !this.collections[commentIndex]) return

    this.collections[commentIndex].title = title
  }

  // Actions

  @Action()
  public async fetchAdverts (): Promise<AdvertType[]> {
    const advertList = await this.service.fetchAdverts()
    this.setCollections(advertList)
    return advertList
  }

  // @Action()
  // public async updateCollection (
  //     advert: AdvertType
  // ): Promise<AdvertType> {
  //     return this.service.updateCollection(advert)
  // }
}
