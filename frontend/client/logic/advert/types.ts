/**
 * These are inner types, they are only used inside the client.
 *
 * If your exchange some information with a server - make it a model.
 * And place it in `/models.ts` file.
 */

import { AdvertType } from '~/logic/advert/models'

// We add rating only on the client (for demo purposes)

export interface  AdvertTypeInterface {
	collections: {
        collections: AdvertType[]
    }
}
